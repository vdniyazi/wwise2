﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicInt2Scr : MonoBehaviour
{
   public AK.Wwise.RTPC intensity_RTPC;
    public GameObject Music;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            AkSoundEngine.SetState("STATE_gamestate", "battle");
            intensity_RTPC.SetGlobalValue(2);
        }
            
    }
}
