﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    public GameObject door;
    AkRoomPortal portal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            door.GetComponent<Animator>().Play("DoorOpen", 0,0f);
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
            door.GetComponent<Animator>().Play("DoorClose", 0, 0f);
    }
}
