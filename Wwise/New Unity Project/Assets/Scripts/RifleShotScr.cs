﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vShooter;
using Invector.vCharacterController;

public class RifleShotScr : MonoBehaviour
{
    public AK.Wwise.Event PlayerShot;
    public vShooterWeapon weapon;
    public AK.Wwise.RTPC ammoRTPC;
    public AK.Wwise.RTPC scopeRTPC;
    vThirdPersonInput thinput;
    public AK.Wwise.Event EmptyEvent;
    float totalammo;
    float currentammo;
    bool scopecheck;

    // Start is called before the first frame update
    void Start()
    {
        weapon = GetComponent<vShooterWeapon>();
        thinput = GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void Shot()
    {
        currentammo = weapon.ammoCount;
        totalammo = weapon.clipSize;
        ammoRTPC.SetValue(gameObject, (currentammo/totalammo)*100);
        PlayerShot.Post(gameObject);

    }

    public void Empty()
    {
        ammoRTPC.SetValue(gameObject, (currentammo / totalammo) * 100);
        if (weapon.ammoCount == 0 && weapon.isAiming)
        {
            if (Input.GetMouseButton(0)) EmptyEvent.Post(gameObject);
        }
    }

    public void ScopeOn()
    {
            scopeRTPC.SetValue(gameObject, 1);      
    }
    public void ScopeOff()
    {
            scopeRTPC.SetValue(gameObject, 0);
    }

}
