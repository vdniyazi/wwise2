﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class AIDeath : MonoBehaviour
{
    vControlAIShooter enemy;
    public AK.Wwise.Event Kill;
    bool check;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponent<vControlAIShooter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy.currentHealth <= 0 && !check)
        {
            Kill.Post(gameObject);
            check = true;
        }
    }


    public void Death()
    {
        
    }
}
