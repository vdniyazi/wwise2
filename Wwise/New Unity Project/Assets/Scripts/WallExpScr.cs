﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallExpScr : MonoBehaviour
{
    public GameObject Wall;
    public AK.Wwise.Event WallExp;
    bool PlayCheck;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayCheck)
        {
            if (Wall.activeSelf == true)
                WallExp.Post(Wall);
            PlayCheck = true;
        }
    }
}
