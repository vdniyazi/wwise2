using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using Invector.vShooter;

public class shoot_script : MonoBehaviour
{
    public vShooterMeleeInput tpinput;
    bool prevButtonPressed = false;
    bool prevReload = false;
    bool eventStarted = false;

    public AK.Wwise.Event shotstart;
    public AK.Wwise.Event shotstop;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        
       var buttonPressed = tpinput.shotInput.GetButton();

        if (buttonPressed != prevButtonPressed)
        {
            if (buttonPressed)
            {
                Debug.Log("EventStart");
                eventStarted = true;
                shotstart.Post(gameObject);

            }
            else if(eventStarted)
            {
                Debug.Log("EventStop");
                eventStarted = false;
                shotstop.Post(gameObject);
            }

        }
        else if (buttonPressed)
        {
            if (tpinput.isReloading && eventStarted)
            {
                Debug.Log("EventStop");
                eventStarted = false;
                shotstop.Post(gameObject);
            }
            else if (!tpinput.isReloading && !eventStarted)
            {
                Debug.Log("EventStart");
                eventStarted = true;
                shotstart.Post(gameObject);
            }
        }


        prevButtonPressed = buttonPressed;
        prevReload = tpinput.isReloading;

    }
}
