﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedScr : MonoBehaviour
{
    public GameObject Cube;
    public float speed = 0;
    public float cube_speed = 0;
    Vector3 lastPosition = Vector3.zero;
    Vector3 CubelastPosition = Vector3.zero;
    public AK.Wwise.RTPC SpeedRTPC;
    Rigidbody l;

    // Start is called before the first frame update
    void Start()
    {
        l = Cube.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var d = Cube.transform.position - transform.position;
        var v0 = Vector3.Dot(gameObject.GetComponent<Rigidbody>().velocity, d.normalized);
        var v8 = Vector3.Dot(l.velocity, d.normalized);
        var alpha = (340 + v0) / (340 + v8);
        SpeedRTPC.SetValue(Cube, alpha);
      //  Debug.Log(SpeedRTPC.GetValue(Cube));
        


        speed = (transform.position - lastPosition).magnitude / Time.deltaTime;
       // Debug.Log("персонаж:" + speed);
        lastPosition = transform.position;

        cube_speed = (Cube.transform.position - CubelastPosition).magnitude / Time.deltaTime;
        CubelastPosition = Cube.transform.position;
       /* Debug.Log("куб:" + cube_speed);
        Debug.Log("v0:" + v0 + ", v8:" + v8);*/






    }
}
