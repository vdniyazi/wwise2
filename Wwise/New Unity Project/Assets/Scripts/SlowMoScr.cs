﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMoScr : MonoBehaviour
{
    public AK.Wwise.RTPC slowmo;
    public AK.Wwise.Event effect;
    bool check;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.X)) && !check)
        {
            slowmo.SetGlobalValue(1);
            effect.Post(gameObject);
            check = true;
        }
            

        if ((Input.GetKeyUp(KeyCode.X)))
        {
            slowmo.SetGlobalValue(0);
            effect.Post(gameObject);
            check = false;
        }
            
    }
}
