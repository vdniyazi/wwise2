﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class MusicScr : MonoBehaviour
{
    public AK.Wwise.RTPC intensity_RTPC;
    public List<GameObject> EnemyList = new List<GameObject>();
    public List<GameObject> EnemyListBase = new List<GameObject>();
    public List<GameObject> EnemyListBase2 = new List<GameObject>();
    uint _combatantsCount = 0;
    float intensity;
    float dead;
    bool deadcheck;
    int i;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var ai in EnemyList)
        {
            var EnemyController = ai.GetComponent<vControlAIShooter>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead//EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
        }
        foreach (var ai in EnemyListBase)
        {
            var EnemyController = ai.GetComponent<vControlAIShooter>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead//EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
        }
        foreach (var ai in EnemyListBase2)
        {
            var EnemyController = ai.GetComponent<vControlAIShooter>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead//EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
        }

    }

    // Update is called once per frame
    void Update()
    {

        uint newCombatantsCount = 0;
        foreach (var enemymob in EnemyList)
        {
            var enemyController = enemymob.GetComponent<vControlAIShooter>();
            if (enemyController.isInCombat)
            {
                ++newCombatantsCount;
                i = 1;
            }     
        }

        foreach (var enemymob in EnemyListBase)
        {
            var enemyController = enemymob.GetComponent<vControlAIShooter>();
            if (enemyController.isInCombat)
            {
                ++newCombatantsCount;
                i = 2;
            }
        }

        foreach (var enemymob in EnemyListBase2)
        {
            var enemyController = enemymob.GetComponent<vControlAIShooter>();
            if (enemyController.isInCombat)
            {
                ++newCombatantsCount;
                i = 3;
            }
        }

        if (newCombatantsCount != _combatantsCount)
        {
            if (_combatantsCount == 0)
            {
                intensity_RTPC.SetValue(gameObject, i);
                AkSoundEngine.SetState("STATE_gamestate", "battle");
                
            }
            
        }


        if (EnemyList.Count == 0 && i!= 2 && i != 3)
        {
            AkSoundEngine.SetState("STATE_gamestate", "exploration");
        }

        if(EnemyListBase2.Count == 0) AkSoundEngine.SetState("STATE_gamestate", "exploration");

    }
    void OnEnemyDead(GameObject DeadEnemy)
    {
        EnemyList.Remove(DeadEnemy);
        EnemyListBase.Remove(DeadEnemy);
        EnemyListBase2.Remove(DeadEnemy);

    }

}
