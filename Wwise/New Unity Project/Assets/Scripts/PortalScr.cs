﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalScr : MonoBehaviour
{
    public GameObject portal;
    private AkRoomPortal AKPortal;
    // Start is called before the first frame update
    void Start()
    {
      
        AKPortal = portal.GetComponent<AkRoomPortal>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PortalOpen()
    {
        AKPortal.Open();
        Debug.Log("Open");
    }
    public void PortalClose()
    {
        AKPortal.Close();
        Debug.Log("Close");
    }
}
