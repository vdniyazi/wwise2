﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vShooter;
using Invector.vCharacterController.AI;

public class WeaponEqScr : MonoBehaviour
{
    public AK.Wwise.Event hangdunev;
    public AK.Wwise.Event assaultev;
    public AK.Wwise.Event shotgunev;
    public GameObject handgun, assault, shotgun;
    vShooterManager shooter;
    public vControlAIShooter engun, enassault, enshotgun;
    bool checkgun, checkassault, checkshotgun;
    

    int a, b, c;
   
    // Start is called before the first frame update
    void Start()
    {
       engun = engun.GetComponent<vControlAIShooter>();
        enassault = enassault.GetComponent<vControlAIShooter>();
        enshotgun = enshotgun.GetComponent<vControlAIShooter>();

    }

    // Update is called once per frame
    void Update()
    { if (a == 0)
        {
            if (handgun == null)
            {
                hangdunev.Post(gameObject);
                a++;
            }
             
        }

        if (b == 0)
        {
            if (assault == null)
            {
                assaultev.Post(gameObject);
                b++;
            }

        }
        if (c == 0)
        {
            if (shotgun == null)
            {
                shotgunev.Post(gameObject);
                c++;
            }

        }

        if (engun.isDead && checkgun == false)
        {
            handgun.SetActive(true);
            checkgun = true;
        }

        if (enassault.isDead && checkassault == false)
        {
            assault.SetActive(true);
            checkassault = true;
        }

        if (enshotgun.isDead && checkshotgun == false)
        {
            shotgun.SetActive(true);
            checkshotgun = true;
        }



    }

    public void Check()
    {
        
    }
}
