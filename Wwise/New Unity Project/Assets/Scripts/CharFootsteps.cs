﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CharFootsteps : MonoBehaviour
{
    public AK.Wwise.Event footstepsevent;
    public AK.Wwise.Event LandEvent;
    public AK.Wwise.Event JumpEvent;
    public AK.Wwise.Event RollEvent;
    public AK.Wwise.Event throwev;
    vThirdPersonInput   tpinput;
    public LayerMask lm;
    string surface;
    public GameObject FootLeft;
    public GameObject FootRight;
    public Animator PlayerAnimator;
    bool leftIsPlaying;
    bool rightIsPlaying;
    public AK.Wwise.RTPC FootVelocityRTPC;
    public AK.Wwise.RTPC AimRTPC;



    // Start is called before the first frame update
    void Start()
    {
        tpinput = gameObject.GetComponent<vThirdPersonInput>();
    }

    // Update is called once per frame
    void Update()
    {
       

        if (PlayerAnimator.GetFloat("foot_left") > 0.01 && !leftIsPlaying)
        {
            if (tpinput.cc.isCrouching)
            {
                FootVelocityRTPC.SetValue(gameObject, 1);
                Footsteps();
                leftIsPlaying = true;
            }
            else
                FootVelocityRTPC.SetValue(gameObject, PlayerAnimator.GetFloat("foot_left")*100);
            Footsteps();
            leftIsPlaying = true;
        }
        else if (PlayerAnimator.GetFloat("foot_left") == 0) leftIsPlaying = false;


            if (PlayerAnimator.GetFloat("foot_right") > 0.01 && !rightIsPlaying)
        {
            if (tpinput.cc.isCrouching)
            {
                FootVelocityRTPC.SetValue(gameObject, 1);
                
                Footsteps();
                rightIsPlaying = true;
            }
            else
            FootVelocityRTPC.SetValue(gameObject, PlayerAnimator.GetFloat("foot_right") * 100);
            Footsteps();
            rightIsPlaying = true;
        }

        else if (PlayerAnimator.GetFloat("foot_right") == 0) rightIsPlaying = false;

//тише шаги на прицеливание
        if (PlayerAnimator.GetBool("IsAiming") == true && !tpinput.cc.isCrouching) AimRTPC.SetValue(gameObject, 0);
        else AimRTPC.SetValue(gameObject, 1);


    }

    void Footsteps()
    {

            SurfaceCheck();

            if (tpinput.cc.inputMagnitude > 1) 
            {
                AkSoundEngine.SetSwitch("SWITCH_locomotion_type","SWITCH_run",gameObject);
                AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                footstepsevent.Post(gameObject); 
            }

            else
            {
                AkSoundEngine.SetSwitch("SWITCH_locomotion_type","SWITCH_walk", gameObject); 
                AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
                footstepsevent.Post(gameObject);
            }
        

    }
    
    void SurfaceCheck()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            surface = hit.collider.tag;
        }
    }

    void Land()
    {
        SurfaceCheck();
        AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
        LandEvent.Post(gameObject);
    }

    void Jump()
    {
        JumpEvent.Post(gameObject);

    }

    void Roll()
    {
        RollEvent.Post(gameObject);
    }

    void grenadethrow()
    {
        throwev.Post(gameObject);
    }

}
