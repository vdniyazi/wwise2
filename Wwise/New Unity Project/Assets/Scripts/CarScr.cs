﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarScr : MonoBehaviour
{
    RCC_CarControllerV3 car;
    public RCC_WheelCollider wheel1;
    public AK.Wwise.Event CarCollision;
    public AK.Wwise.Event EngineSound;
    public AK.Wwise.Event GearShift;
    public AK.Wwise.Event CarLand;
    public AK.Wwise.Event Tires;

    public AK.Wwise.RTPC RPM;
    public AK.Wwise.RTPC speed;
    public AK.Wwise.RTPC skid;

    public Camera carcamera;
    bool gearcheck, cameracheck, stopgearcheck, wheelcheck, skidcheck;
    int prevgear,currentgear;
    public LayerMask lm;
    string surface;
    public List<RCC_WheelCollider> bumpforce = new List<RCC_WheelCollider>();
    

    // Start is called before the first frame update
    void Start()
    {
        car = GetComponent<RCC_CarControllerV3>();
        wheel1 = GetComponent<RCC_WheelCollider>();

    }

    // Update is called once per frame
    void Update()
    { if (carcamera.isActiveAndEnabled == true) 
        {
            if (!cameracheck) 
            { 
                EngineSound.Post(gameObject);
                Tires.Post(gameObject);
                cameracheck = true;
            }
            Gear();
            Accel();
        }
        if (car.engineRPM <= 1000) RPM.SetValue(gameObject, 1000);
        else RPM.SetValue(gameObject, car.engineRPM);
        
        speed.SetValue(gameObject, car.speed);

        SurfaceCheck();
        
        Bounce();
        Skid();


    }


    public void Gear()
    {


        currentgear = car.currentGear;
        if (prevgear < currentgear)
        {
            stopgearcheck = false;
            if (!gearcheck)
            {
                GearShift.Post(gameObject);
                gearcheck = true;
            }
            
            prevgear = currentgear;

        }
        else gearcheck = false;

        if (prevgear > currentgear)
        {
            if (!stopgearcheck)
            {
                GearShift.Post(gameObject);
                stopgearcheck = true;
            }

            prevgear = currentgear;

        }


    }

    public void Accel()
    {
        if (Input.GetKey(KeyCode.W)) AkSoundEngine.SetRTPCValue("ext_car_rev", 1, gameObject);
        else if (Input.GetKeyUp(KeyCode.W)) AkSoundEngine.SetRTPCValue("ext_car_rev", 0, gameObject);
    }

    void SurfaceCheck()
    {
        if (Physics.Raycast(gameObject.transform.position, Vector3.down, out RaycastHit hit, 1f, lm))
        {
            surface = hit.collider.tag;
        }
        AkSoundEngine.SetSwitch("SWITCH_surface_type", surface, gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.activeSelf == true) CarCollision.Post(gameObject);

    }

    void Bounce()
    {
        foreach (RCC_WheelCollider wheel in bumpforce)
        {
            if (wheel.bumpForce >= 5000f) CarLand.Post(wheel.gameObject);

        }
        
    }
    void Skid()
    {
        
        foreach (RCC_WheelCollider wheel in bumpforce) {
            skidcheck = wheel.isSkidding();

            /* for (int i = 0; i < wheel.allWheelColliders.Count; i++)
          {

              if (wheel.allWheelColliders[i].totalSlip > wheel.physicsFrictions[wheel.groundIndex].slip)
                  skidcheck = true;

              else skidcheck = false;
          }
           // Debug.Log(skidcheck);*/


            if (skidcheck) skid.SetValue(gameObject, 2);
       // else if (Input.GetKey(KeyCode.Space) && skidcheck && car.speed > 40) skid.SetValue(gameObject, 2);
        else if (!skidcheck) skid.SetValue(gameObject, 1);
        }

    }
    

    }
