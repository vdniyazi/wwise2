﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;
using UnityEngine.SceneManagement;

public class CharLowHP : MonoBehaviour
{
   public AK.Wwise.Event HealthLow;
    public AK.Wwise.Event HealthHigh;
    public AK.Wwise.Event RestartEv;
    public vThirdPersonInput chinput;
    bool heartisplaying;
    float health;
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (chinput.cc.currentHealth < 90 && !heartisplaying)
        {
            HealthLow.Post(gameObject);
            heartisplaying = true;
           
        }
        else if (chinput.cc.currentHealth > 90)
        {
            HealthHigh.Post(gameObject);
            heartisplaying = false;
        }

        if (chinput.cc.currentHealth <= 0)
        {
            HealthHigh.Post(gameObject);
            Invoke("Restart", 2f);
            heartisplaying = true;
        }
            


    }

    void Restart()
    {
        RestartEv.Post(gameObject);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
 
}
