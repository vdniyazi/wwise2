﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeliTriggerScr : MonoBehaviour
{
    public Rigidbody Heli;
    public GameObject HeliObj;
    public AK.Wwise.Event Sound;
    bool check;
    // Start is called before the first frame update
    void Start()
    {
        Heli = Heli.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !check)
        {
            Heli.velocity = new Vector3(0, 0, -60);
            Sound.Post(HeliObj);
            check = true;
        }
    }
}
