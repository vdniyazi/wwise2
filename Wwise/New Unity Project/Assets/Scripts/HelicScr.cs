﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicScr : MonoBehaviour
{
    float speed;
    public Rigidbody rb;
    public GameObject blades;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        speed = -60f;
        
    }

    // Update is called once per frame
    void Update()
    {
        var p = gameObject.transform.position;
        //gameObject.transform.position += gameObject.transform.forward * speed * Time.deltaTime;
        //rb.velocity = new Vector3(0, 0, speed);
        blades.transform.Rotate(0f, 100f, 0f, Space.Self);

    }
}
