using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class spawn_manager : MonoBehaviour
{
   // public GameObject[] enemyPrefabs; // Создаем массив для префабов врагов
    public float rangeX = 5; // координаты для появления. Можно вводить ниже просто цифрами без переменных
    public float positionZ = 5; // позиция по оси Z
    public int numberOfenemies = 0; // в этой переменной будет храниться количество всех врагов (добавляется +1, когда враги появляются, а -1, когда враги умирают)
    public vWaypointArea waypoint; // В это поле закидываем объект WayPointArea - точки перемещения по карте AI
    private float Level;
   public AK.Wwise.RTPC intensity_RTPC;
   public List<GameObject> enemyList = new List<GameObject>(); // создаем список для хнранени
    uint _combatantsCount = 0;
    float n, prev_intensity, intensity;

    void Start()
    { foreach(var ai in enemyList) 
        { 
        var EnemyController = ai.GetComponent<vControlAIShooter>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead//EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
        }
        
    }





    private void Update()
    {
       
        uint newCombatantsCount = 0;
        foreach(var enemymob in enemyList)
        {
            var enemyController =  enemymob.GetComponent<vControlAIShooter>();
            if (enemyController.isInCombat)
            {
                ++newCombatantsCount;
            }
        }

        if (newCombatantsCount != _combatantsCount)
        {
            if (_combatantsCount == 0)
            {
                AkSoundEngine.SetState("STATE_gamestate", "battle");
            }
            else if (newCombatantsCount == 0)
            {
                AkSoundEngine.SetState("STATE_gamestate", "exploration");
                intensity = 0;
            }
            intensity = newCombatantsCount;

            if (intensity < prev_intensity) intensity = prev_intensity;
            else prev_intensity = intensity;
            if (intensity < 1)
            {
                intensity = 1;
            } else if (intensity > 3)
            {
                intensity = 3;
            }

            intensity_RTPC.SetValue(gameObject,intensity);
            Level = intensity;
            _combatantsCount = newCombatantsCount;
            
        }
    }

    void OnEnemyDead(GameObject DeadEnemy)
    {
        enemyList.Remove(DeadEnemy);
        
    }

    private void OnGUI()
    {
        GUILayout.Space(100);
        GUILayout.Label("Game State: " +  (_combatantsCount > 0 ? "Combat" : "Exploration" ));
        GUILayout.Label("Number of Enemies: ");
        GUILayout.Label("Intensity: " + Level);
    }
}
