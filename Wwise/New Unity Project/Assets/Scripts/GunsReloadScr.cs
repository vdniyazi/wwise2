﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsReloadScr : MonoBehaviour
{
    public AK.Wwise.Event HandgunReload;
    public AK.Wwise.Event ShotgunReload;
    public AK.Wwise.Event AssaultReload;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void pistol()
    {
        HandgunReload.Post(gameObject);
    }

    void shotgun()
    {
        ShotgunReload.Post(gameObject);
      
    }

    void assault()
    {
        AssaultReload.Post(gameObject);
    }
}
