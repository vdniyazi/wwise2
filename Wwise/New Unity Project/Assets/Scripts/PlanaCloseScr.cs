﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanaCloseScr : MonoBehaviour
{
    public GameObject Door;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    { 
      

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Door.GetComponent<Animator>().Play("PlaneClose", 0, 0f);

        }
    }

}
