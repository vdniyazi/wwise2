﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCollision : MonoBehaviour
{
    public List<GameObject> bricklist = new List<GameObject>();
    public AK.Wwise.Event DebrisEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
       
            if (collision.gameObject.tag == "Debris")
            DebrisEvent.Post(gameObject);
    }
}
