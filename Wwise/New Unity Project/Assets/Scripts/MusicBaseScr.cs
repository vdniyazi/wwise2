﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController.AI;

public class MusicBaseScr : MonoBehaviour
{
    public AK.Wwise.RTPC intensity_RTPC;
    public List<GameObject> enemyList = new List<GameObject>();
    uint _combatantsCount = 0;
    float intensity;
    float dead;
    bool deadcheck;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var ai in enemyList)
        {
            var EnemyController = ai.GetComponent<vControlAIShooter>(); // подключаемся к контроллеру AI
            EnemyController.onDead.AddListener(OnEnemyDead); // здесь мы следим за функцией onDead - оставляем коллбек AddListener и когда она производится мы запускаем метод OnEnemyDead//EnemyController.waypointArea = waypoint; // Передаем заспауненному префабу точки движения
        }

    }

    // Update is called once per frame
    void Update()
    {

        uint newCombatantsCount = 0;
        foreach (var enemymob in enemyList)
        {
            var enemyController = enemymob.GetComponent<vControlAIShooter>();
            if (enemyController.isInCombat)
            {
                ++newCombatantsCount;
            }     
        }

        if (newCombatantsCount != _combatantsCount)
        {
            if (_combatantsCount == 0)
            {
                AkSoundEngine.SetState("STATE_gamestate", "battle");
                intensity_RTPC.SetValue(gameObject, 2);
            }
            
        }

    }
    void OnEnemyDead(GameObject DeadEnemy)
    {
        enemyList.Remove(DeadEnemy);

    }

}
