﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothScr : MonoBehaviour
{
    public Collider BodyCollider;
    public Animator PlayerAnimator;
    public AK.Wwise.Event ClothEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == BodyCollider)
        {
            ClothEvent.Post(gameObject);
        }
    }
}
