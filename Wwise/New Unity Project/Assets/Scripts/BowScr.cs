﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class BowScr : MonoBehaviour
{
    public AK.Wwise.RTPC PowerChargerRTPC;
    public Animator PlayerAnimator;
    public AK.Wwise.Event Bow;
    bool PlayCheck;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
     if (PlayerAnimator.GetFloat("PowerCharger") > 0.01)
        {
            if (!PlayCheck)
            { 
            Bow.Post(gameObject);
                PlayCheck = true;
            }
            PowerChargerRTPC.SetValue(gameObject, PlayerAnimator.GetFloat("PowerCharger"));
        }
        else if (PlayerAnimator.GetFloat("PowerCharger") == 0)
        {
            Bow.Stop(gameObject);
            PlayCheck = false;
        }
    }
}
