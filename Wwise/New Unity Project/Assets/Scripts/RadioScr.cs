﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioScr : MonoBehaviour
{
    public GameObject trigger,mouth;
    public AK.Wwise.Event Event;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == trigger) Event.Post(mouth);
    }
}
