﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vShooter;

public class CameraHightScr : MonoBehaviour
{
    public AK.Wwise.RTPC CameraHeightRTPC;
    public GameObject Camera;
    public Animator PA;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PA.GetBool("IsAiming") == false)
        CameraHeightRTPC.SetValue(gameObject,Camera.transform.position.y);
        
    }
}
