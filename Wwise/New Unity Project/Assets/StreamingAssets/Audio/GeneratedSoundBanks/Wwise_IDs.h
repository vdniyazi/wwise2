/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AI_HANDGUN = 2118773985U;
        static const AkUniqueID AI_RIFLE = 2053475092U;
        static const AkUniqueID AI_SHOTGUN = 1670858472U;
        static const AkUniqueID ASSAULTEQ = 1751829620U;
        static const AkUniqueID CHARDAMAGE = 3260717762U;
        static const AkUniqueID DEATH = 779278001U;
        static const AkUniqueID DEATHWALL = 593189053U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID HANDGUNEQ = 1699554210U;
        static const AkUniqueID HEARTBEAT_START = 201282074U;
        static const AkUniqueID HEARTBEAT_STOP = 344202674U;
        static const AkUniqueID JUMP = 3833651337U;
        static const AkUniqueID PLAY_AMB_FIRE = 2767852325U;
        static const AkUniqueID PLAY_AMB_HELICOPTER = 3540515310U;
        static const AkUniqueID PLAY_ASSAULTRELOAD = 2319339896U;
        static const AkUniqueID PLAY_CARCOLLISION = 1463024416U;
        static const AkUniqueID PLAY_CARLAND = 4041951679U;
        static const AkUniqueID PLAY_CLICK = 311910498U;
        static const AkUniqueID PLAY_CLOTH = 128342314U;
        static const AkUniqueID PLAY_DEATH = 1172822028U;
        static const AkUniqueID PLAY_DECALCAR = 543294801U;
        static const AkUniqueID PLAY_ENGINE = 639345804U;
        static const AkUniqueID PLAY_GEAR_SHIFT = 1051256182U;
        static const AkUniqueID PLAY_GRENADE = 2405798708U;
        static const AkUniqueID PLAY_GRENADETHROW = 496299996U;
        static const AkUniqueID PLAY_HANDGUN = 3874872701U;
        static const AkUniqueID PLAY_HITMARKER = 1731259975U;
        static const AkUniqueID PLAY_INDOOR = 2430332871U;
        static const AkUniqueID PLAY_LAND = 4285282925U;
        static const AkUniqueID PLAY_LEVEL_MUSIC = 557932600U;
        static const AkUniqueID PLAY_OCCTEST = 3240468179U;
        static const AkUniqueID PLAY_OUTDOOR = 304929234U;
        static const AkUniqueID PLAY_PIPEFLAME = 2318200527U;
        static const AkUniqueID PLAY_RADIOTALK = 516562205U;
        static const AkUniqueID PLAY_RIFLE = 1920272688U;
        static const AkUniqueID PLAY_ROLL = 2719919427U;
        static const AkUniqueID PLAY_SHOTGUN = 992244U;
        static const AkUniqueID PLAY_SHOTGUNRELOAD = 1780972667U;
        static const AkUniqueID PLAY_SLOWMO = 4156268665U;
        static const AkUniqueID PLAY_TEST_GUN = 2940530245U;
        static const AkUniqueID PLAY_TIRES = 142669115U;
        static const AkUniqueID PLAY_TV_NEWS = 2236550042U;
        static const AkUniqueID PLAY_VENT_FAN = 717929701U;
        static const AkUniqueID PLAY_WALLBREAK = 3767549391U;
        static const AkUniqueID PLAY_WALLDEBRIS = 175830035U;
        static const AkUniqueID SHOTGUNEQ = 3606981435U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace STATE_AMB
        {
            static const AkUniqueID GROUP = 392285017U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_INDOOR = 3825359022U;
                static const AkUniqueID STATE_OUTDOOR = 3990655221U;
            } // namespace STATE
        } // namespace STATE_AMB

        namespace STATE_GAMESTATE
        {
            static const AkUniqueID GROUP = 1072505152U;

            namespace STATE
            {
                static const AkUniqueID BATTLE = 2937832959U;
                static const AkUniqueID EXPLORATION = 2582085496U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace STATE_GAMESTATE

        namespace STATE_LOWHP
        {
            static const AkUniqueID GROUP = 1519244355U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID STATE_HEARTBEAT_START = 2900964804U;
                static const AkUniqueID STATE_HEARTBEAT_STOP = 3434497400U;
            } // namespace STATE
        } // namespace STATE_LOWHP

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SWITCH_GAME_INTENSITY
        {
            static const AkUniqueID GROUP = 1371057612U;

            namespace SWITCH
            {
                static const AkUniqueID BATTLE_INT1 = 721392230U;
                static const AkUniqueID BATTLE_INT2 = 721392229U;
                static const AkUniqueID BATTLE_INT3 = 721392228U;
            } // namespace SWITCH
        } // namespace SWITCH_GAME_INTENSITY

        namespace SWITCH_GEAR
        {
            static const AkUniqueID GROUP = 3747976225U;

            namespace SWITCH
            {
                static const AkUniqueID GEAR_OFF = 3153319926U;
                static const AkUniqueID GEAR_ON = 3181757496U;
            } // namespace SWITCH
        } // namespace SWITCH_GEAR

        namespace SWITCH_LOCOMOTION_TYPE
        {
            static const AkUniqueID GROUP = 3794968246U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_RUN = 3818208631U;
                static const AkUniqueID SWITCH_WALK = 3627811739U;
            } // namespace SWITCH
        } // namespace SWITCH_LOCOMOTION_TYPE

        namespace SWITCH_SKID
        {
            static const AkUniqueID GROUP = 206914745U;

            namespace SWITCH
            {
                static const AkUniqueID SKID_OFF = 1873450750U;
                static const AkUniqueID SKID_ON = 686394880U;
            } // namespace SWITCH
        } // namespace SWITCH_SKID

        namespace SWITCH_SURFACE_TYPE
        {
            static const AkUniqueID GROUP = 2818941756U;

            namespace SWITCH
            {
                static const AkUniqueID SWITCH_SURFACE_CONCRETE = 3294452117U;
                static const AkUniqueID SWITCH_SURFACE_METAL = 3131161413U;
                static const AkUniqueID SWITCH_SURFACE_SAND = 3129684586U;
            } // namespace SWITCH
        } // namespace SWITCH_SURFACE_TYPE

        namespace SWITCH_TAIL
        {
            static const AkUniqueID GROUP = 2035271718U;

            namespace SWITCH
            {
                static const AkUniqueID INDOOR = 340398852U;
                static const AkUniqueID OUTDOOR = 144697359U;
            } // namespace SWITCH
        } // namespace SWITCH_TAIL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID DENSITY_FA_SSGRAIN = 2715217995U;
        static const AkUniqueID EXT_AMMO = 2479973135U;
        static const AkUniqueID EXT_CAMERA_HEIGHT = 1862344870U;
        static const AkUniqueID EXT_CAR_REV = 4106173823U;
        static const AkUniqueID EXT_CHAR_HEALTH = 90821858U;
        static const AkUniqueID EXT_FOOT_VELOCTIY = 3990589487U;
        static const AkUniqueID EXT_POWER_CHARGER = 1101893347U;
        static const AkUniqueID EXT_RPM = 2312309248U;
        static const AkUniqueID EXT_SCOPE = 3155003175U;
        static const AkUniqueID EXT_SKID = 2890078768U;
        static const AkUniqueID EXT_SPEED = 3009633574U;
        static const AkUniqueID EXT_SPEED_CAR = 971590229U;
        static const AkUniqueID EXT_TENT_FILTER = 823836655U;
        static const AkUniqueID EXT_WEAPONAI = 3595428923U;
        static const AkUniqueID IGN_ENG_EXH_POSITION = 1776289502U;
        static const AkUniqueID IGN_GEARBOX_THROTTLE = 3744760799U;
        static const AkUniqueID IGN_HARLEY_GRAIN_LOOP_BLEND = 1573260114U;
        static const AkUniqueID IGN_LOAD = 1046277832U;
        static const AkUniqueID IGN_RPM = 2202868459U;
        static const AkUniqueID IGNITERSYNTH_REV = 1355046805U;
        static const AkUniqueID IGNITERSYNTH_STATE = 3696491809U;
        static const AkUniqueID IMMERSION_FA_SSGRAIN = 2481728872U;
        static const AkUniqueID INSTRUMENT_FA_SSGRAIN = 2317409760U;
        static const AkUniqueID INT_AIM = 4009785546U;
        static const AkUniqueID INT_AZIMUTH = 1865170437U;
        static const AkUniqueID INT_CONE = 3796143798U;
        static const AkUniqueID INT_DISTANCE = 4019816542U;
        static const AkUniqueID INT_ELEVATION = 1650238500U;
        static const AkUniqueID INT_OCCLUSION = 558787870U;
        static const AkUniqueID INT_SLOWMO = 586190280U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID PROXIMITY_FA_SSGRAIN = 1791284502U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID RPM_FA_SSGRAIN = 1656280998U;
        static const AkUniqueID RTPC_EXT_GAME_INTENSITY = 344389039U;
        static const AkUniqueID SIMULATION_FA_SSGRAIN = 2428833394U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID _10_VO = 2061687904U;
        static const AkUniqueID _20_WEAPONS = 3112982797U;
        static const AkUniqueID _30_AI = 1492234017U;
        static const AkUniqueID _40_CAR = 3652683230U;
        static const AkUniqueID _50_CHARACTER = 3133779730U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID AMBIENCE_BUS = 174546974U;
        static const AkUniqueID AUX = 983310469U;
        static const AkUniqueID BATTLE_BUS = 814022936U;
        static const AkUniqueID DEBRIS = 459611840U;
        static const AkUniqueID EFFECTS = 1942696649U;
        static const AkUniqueID HDR = 931844945U;
        static const AkUniqueID INDOOR = 340398852U;
        static const AkUniqueID LOCOMOTION = 556887514U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID OUTDOOR = 144697359U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID AUX_REVERB_HANGAR = 2146534206U;
        static const AkUniqueID AUX_REVERB_INDOOR = 1858202170U;
        static const AkUniqueID AUX_REVERB_OUTDOOR = 3682646185U;
        static const AkUniqueID AUX_REVERB_PLANE = 52680443U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
